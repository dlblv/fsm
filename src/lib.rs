//! # FSM - Finite State Machine
//!
//! Usage
//! ```rust
//! use fsm::{FsmSpec, Fsm};
//!
//! #[derive(Debug, PartialEq, Eq)]
//! enum State {
//!     A,
//!     B,
//!     C,
//! }
//!
//! enum Event {
//!     Next,
//!     Prev,
//! }
//!
//! struct MyFsm {
//!     event_count: usize,
//! }
//!
//! impl FsmSpec for MyFsm {
//!     type Event = Event;
//!     type State = State;
//!
//!     fn on_enter(&mut self, state: &Self::State) {
//!         match state {
//!             State::A => println!("Entered state A!"),
//!             State::B => println!("Entered state B!"),
//!             State::C => println!("Entered state C!"),
//!         }
//!     }
//!     fn on_exit(&mut self, state: &Self::State) {
//!         match state {
//!             State::A => println!("Exited state A!"),
//!             State::B => println!("Exited state B!"),
//!             State::C => println!("Exited state C!"),
//!         }
//!     }
//!     fn transitions(&mut self, state: Self::State, event: Self::Event) -> Self::State {
//!         self.event_count += 1;
//!         match (state, event) {
//!             (State::A, Event::Next) => State::B,
//!             (State::B, Event::Next) => State::C,
//!             (State::C, Event::Next) => State::A,
//!             (State::A, Event::Prev) => State::C,
//!             (State::B, Event::Prev) => State::A,
//!             (State::C, Event::Prev) => State::B,
//!         }
//!     }
//! }
//!
//! let mut fsm = Fsm::new(State::A, MyFsm { event_count: 0 });
//! assert_eq!(fsm.state(), &State::A);
//! assert_eq!(fsm.ctx().event_count, 0);
//!
//! fsm.handle_event(Event::Next);
//! assert_eq!(fsm.state(), &State::B);
//! assert_eq!(fsm.ctx().event_count, 1);
//!
//! fsm.handle_event(Event::Next);
//! assert_eq!(fsm.state(), &State::C);
//! assert_eq!(fsm.ctx().event_count, 2);
//!
//! fsm.handle_event(Event::Prev);
//! assert_eq!(fsm.state(), &State::B);
//! assert_eq!(fsm.ctx().event_count, 3);
//! ```
#![no_std]

/// Setup parameters of your FSM
pub trait FsmSpec {
    /// Type representing machine states
    type State;
    /// Type representing events handled by FSM
    type Event;

    /// Describes your machine transitions
    fn transitions(&mut self, state: Self::State, event: Self::Event) -> Self::State;
    /// What happens when specific state is entered
    fn on_enter(&mut self, _state: &Self::State) {}
    /// What happens when specific state is entered
    fn on_exit(&mut self, _state: &Self::State) {}
}

/// FSM, ready to operate upon your context
pub struct Fsm<F: FsmSpec> {
    current_state: Option<F::State>,
    ctx: F,
}

impl<F: FsmSpec> Fsm<F> {
    /// Create an FSM with an initial state
    pub fn new(initial_state: F::State, mut ctx: F) -> Self {
        ctx.on_enter(&initial_state);
        Self {
            current_state: Some(initial_state),
            ctx,
        }
    }

    /// Pass event into FSM
    pub fn handle_event(&mut self, event: F::Event) {
        let state = self.current_state.take().unwrap();
        self.ctx.on_exit(&state);
        let new_state = self.ctx.transitions(state, event);
        self.ctx.on_enter(&new_state);
        self.current_state = Some(new_state);
    }

    /// Get current machine state
    pub fn state(&self) -> &F::State {
        self.current_state.as_ref().unwrap()
    }

    /// Get FSM context
    pub fn ctx(&self) -> &F {
        &self.ctx
    }

    /// Get mutable FSM context
    pub fn ctx_mut(&mut self) -> &mut F {
        &mut self.ctx
    }
}

impl<F> Default for Fsm<F>
where
    F: FsmSpec + Default,
    F::State: Default,
{
    fn default() -> Self {
        let mut ctx = F::default();
        let state = F::State::default();
        ctx.on_enter(&state);
        Self {
            current_state: Some(state),
            ctx,
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test() {
        #[derive(Debug, Default)]
        struct MyFsm {
            call_num: usize,
            x: u8,
        }

        #[derive(Debug, PartialEq, Eq, Default)]
        enum State {
            #[default]
            A,
            B,
            C,
        }

        impl FsmSpec for MyFsm {
            type Event = ();
            type State = State;

            fn transitions(&mut self, state: Self::State, _event: ()) -> Self::State {
                self.call_num += 1;
                match state {
                    State::A => State::B,
                    State::B => State::C,
                    State::C => State::A,
                }
            }

            fn on_enter(&mut self, state: &Self::State) {
                match state {
                    State::A => self.x = 100,
                    State::B => self.x = 42,
                    State::C => self.x = 0,
                }
            }
        }

        let mut fsm = Fsm::<MyFsm>::default();
        assert_eq!(fsm.state(), &State::A);
        assert_eq!(fsm.ctx().call_num, 0);
        assert_eq!(fsm.ctx().x, 100);

        fsm.handle_event(());
        assert_eq!(fsm.state(), &State::B);
        assert_eq!(fsm.ctx().call_num, 1);
        assert_eq!(fsm.ctx().x, 42);

        fsm.handle_event(());
        assert_eq!(fsm.state(), &State::C);
        assert_eq!(fsm.ctx.call_num, 2);
        assert_eq!(fsm.ctx().x, 0);

        fsm.handle_event(());
        assert_eq!(fsm.state(), &State::A);
        assert_eq!(fsm.ctx().call_num, 3);
        assert_eq!(fsm.ctx().x, 100);
    }
}
